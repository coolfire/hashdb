#!/usr/bin/env ruby
# frozen_string_literal: true

require 'digest'
require 'leveldb'

@buffersize = 10000
nthreads    = 20

@md5db    = LevelDB::DB.new 'md5.db'
@sha1db   = LevelDB::DB.new 'sha1.db'
@sha256db = LevelDB::DB.new 'sha256.db'

# Define our read buffer
@buffer = SizedQueue.new @buffersize

# Function to constantly fill our read buffer
def fill_buffer
  File.open('passwordlist.txt', 'r').each_line do |line|
    @buffer.push line.strip
  end
end

# Function to constantly drain our read buffer
def process_buffer(b = [])
  loop do
    password = @buffer.pop
    break if password.nil?

    b.push(
      password: password,
      md5:      (Digest::MD5.hexdigest password),
      sha1:     (Digest::SHA1.hexdigest password),
      sha256:   (Digest::SHA256.hexdigest password)
    )

    # Submit if we've filled our buffer
    if b.size == @buffersize
      submit_results b
      b.clear
    end
  end

  # Submit a partially filled buffer if we're at the end
  submit_results b unless b.size.zero?
end



# Submit calculated hashes to leveldb
def submit_results(results)
  # A little progress indication
  puts results.first

  md5b    = @md5db.batch
  sha1b   = @sha1db.batch
  sha256b = @sha256db.batch
  results.each do |r|
    md5b.put    r[:md5],    r[:password]
    sha1b.put   r[:sha1],   r[:password]
    sha256b.put r[:sha256], r[:password]
  end
  md5b.write!
  sha1b.write!
  sha256b.write!
end

# Fill password buffer
producer = Thread.new { fill_buffer }

# Start processing passwords
consumers = []
nthreads.times do
  consumers.push(Thread.new { process_buffer })
end

# Wait for producer to finish
producer.join
puts 'No more passwords in file'

# Signal workers to finish
nthreads.times do
  @buffer.push nil
end

# Wait until all workers are done
consumers.each(&:join)
puts 'All done!'
